FROM openjdk:8
ADD build/libs/soaint-0.0.1-SNAPSHOT-application.jar soaint.jar
EXPOSE 8080
ENTRYPOINT [ "java", "-jar", "soaint.jar" ]
