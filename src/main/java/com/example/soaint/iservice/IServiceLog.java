package com.example.soaint.iservice;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.soaint.entity.LogEntity;
import com.example.soaint.repository.LogRepository;
import com.example.soaint.service.ServiceLog;

@Service
public class IServiceLog implements ServiceLog{
	
	@Autowired
	LogRepository logRepository;

	@Override
	public List<LogEntity> obtenerLista() {
		return logRepository.findAll();
	}

	@Override
	public LogEntity registrarlog(LogEntity log) {
		return logRepository.save(log);
	}

}
