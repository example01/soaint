package com.example.soaint.dto;

import java.util.Date;
import java.util.UUID;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class LogDTO {
	private UUID pkIdLog;
	private String tipo;
	private String mensaje;
	private Date dtfechaModificacion;

}
