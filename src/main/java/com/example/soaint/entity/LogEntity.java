package com.example.soaint.entity;




import java.util.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
@Table(name="log")
@NamedQuery(name = "LogEntity.findAll", query = "SELECT a FROM LogEntity a")
public class LogEntity {
	
	@Id
    @Column(name = "pk_id", updatable = false, nullable = false, columnDefinition = "BINARY(16)")
	private UUID pkIdLog = UUID.randomUUID();
	
	private String tipo;
	
	private String mensaje;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "dt_creacion")
	private Date dtfechaModificacion;

}
