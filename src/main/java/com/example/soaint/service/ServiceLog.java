package com.example.soaint.service;

import java.util.List;

import com.example.soaint.entity.LogEntity;

public interface ServiceLog {

	public List<LogEntity> obtenerLista();

	public LogEntity registrarlog(LogEntity abono);
}
