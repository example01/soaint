package com.example.soaint.example;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.soaint.dto.LogDTO;
import com.example.soaint.entity.LogEntity;
import com.example.soaint.service.ServiceLog;
import com.github.dozermapper.core.Mapper;

import lombok.extern.slf4j.Slf4j;
@Slf4j
@RestController
@RequestMapping("/api/v1/soaint/log")
public class DemoSolucion {

	@Autowired
    ServiceLog serviceLog;
	
	@Autowired
	private Mapper mapper;
	
	@GetMapping("/getAll")
	public @ResponseBody ResponseEntity<List<LogDTO>> obtenerlog() throws Exception{
		log.info("Obtener log");
		List<LogEntity> logs = serviceLog.obtenerLista();
		if(logs.isEmpty()) {
			throw new Exception("No se encontro log");
		}
		List<LogDTO> logsdto = new ArrayList<>();
		for (LogEntity mp : logs) {
			logsdto.add(mapper.map(mp, LogDTO.class));
		}
		return ResponseEntity.status(HttpStatus.OK).body(logsdto);
	}

	@PostMapping("/add")
	public @ResponseBody ResponseEntity<LogDTO> agregarElemento(@RequestParam String tipo) throws Exception{
		log.info("Agregar log");
		log.info("tipo 1 : console");
		log.info("tipo 2 : file");
		log.info("tipo 3 : database");
		

		
		if("3".equalsIgnoreCase(tipo)) {
			LogDTO log = new LogDTO();
			log.setPkIdLog(UUID.randomUUID());
			log.setMensaje("Prueba log : " + tipo);
			log.setTipo(tipo);
			log.setDtfechaModificacion(new Date());
			serviceLog.registrarlog(mapper.map(log, LogEntity.class));
		}
		
		if("".equalsIgnoreCase(tipo)) {
			throw new Exception("error al registrar log ");
		}
		return ResponseEntity.status(HttpStatus.OK).build();

	}
	
}
