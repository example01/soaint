CREATE TABLE log (
		pk_id VARCHAR (255) FOR BIT DATA NOT NULL,
		tipo VARCHAR(10),
		mensaje VARCHAR(500) NOT NULL,
		id_valor VARCHAR(255),
		dt_creacion TIMESTAMP,
		PRIMARY KEY (pk_id)
	);
